# Internship workflow - Damien MAURE

This git reposit is here to explain the most important parts of the workflow I created during my internship at IGE (CryoDyn team) with Fabien Gillet-Chaulet, Nicolas Champollion and Samuel Cook, between 03/21 and 09/21.
It was used to model all the glaciers of the Mont-Blanc massif and their evolution using Elmer/Ice.
As it is, It can be used on a local computer if you want to model one glacier or small ensembles, but some additional steps (TODO) will be needed for transfering data/scripts to a computing center for calculus of large ensembles.

For the moment, this reposit is still work in progress. I plan to improve it until everything is nice and clear.
If you are trying to use it and there is something not understandable or not working, or if you'd like more infos, please send me a email at *damien.maure@gmail.com*.


# Contents

*steps/* contains the core of the workflow.

*data/* contains an archive with all the input data used by the workflow, to be unzipped once you've downloaded the whole reposit.

