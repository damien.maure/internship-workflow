#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  1 08:27:08 2021

@author: maured
"""

import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
import os
import rioxarray # rasterio xarray extension
import rasterio  # access to geospatial raster data
import pandas as pd
import seaborn as sns # Statistical Data Visualization

def rmse(x):
    
    x = x.values.flatten()
    #we delete values with nan and infinites
    x = x[~np.isnan(x)]
    x = x[x>-10e20]
    if len(x)==0:
        return 0
    return np.sqrt(sum(x**2)/len(x))

def bias(x):
    
    x = x.values.flatten()
    #we delete values with nan and infinites
    x = x[~np.isnan(x)]
    x = x[x>-10e20]
    if len(x)==0:
        return 0
    return sum(x)/len(x)


ref_speed=xr.open_dataset('../../data/obs_speed/GLIDATASET_ALPS.nc')
ref_speed = ref_speed.drop('mapping')

ref_speed = ref_speed.rio.write_crs("EPSG:32632")

model_dict = {'r_model.nc':'Millan','composite.nc':'Composite','results_model_1.nc':'Farinotti','results_model_2.nc':'Frey','results_model_3.nc':'Maussion','results_model_4.nc':'Fürst'}
best_dfs = pd.DataFrame(columns = ['C', 'A','model','rmse','bias','score'])

for glacier in os.listdir('outputs'):
    df = pd.DataFrame(columns = ['C', 'A','model','rmse','bias','score'])    
    
    j=0
    i=0
    for A in os.listdir('outputs/'+glacier):
        i+=1
        for C in np.sort(os.listdir('outputs/'+glacier+'/'+A)):
            for model in np.sort(os.listdir('outputs/'+glacier+'/'+A+'/'+C)):
                print(j)
                #try:
                if model[-3:]!='xml':
                    model_ds=xr.open_dataset('outputs/'+glacier+'/'+A+'/'+C+'/'+model)
                    j+=1
                    if j==1:
                        lon_new=model_ds.lon.values
                        lat_new=model_ds.lat.values
                        fin_ref_speed = ref_speed.reindex(y=lat_new, x=lon_new, method='nearest')
                        v_x = fin_ref_speed.vx
                        v_y = fin_ref_speed.vy
                        V_ref = np.sqrt(fin_ref_speed.vx**2 + fin_ref_speed.vy**2)
                        #STD = np.sqrt(fin_ref_speed.stdx**2 + fin_ref_speed.stdy**2)
                    v=model_ds.where(model_ds.magnitude>0.01)
                    v=v.rename({'lat':'y','lon':'x'})
                    err = ((v.vx-v_x)*v_x + (v.vy-v_y)*v_y)/V_ref
                    rmse_tot = rmse(err)
                    bias_tot = bias(err)
                    
                    score = rmse_tot #np.sqrt((5*bias_tot)**2+rmse_tot**2)
                    
                    df.loc[j]=[C,A,model_dict[model],rmse_tot,bias_tot,score]
    
    plt.figure()              
    grid = sns.FacetGrid(df, row = 'C',col='A',hue='model',aspect=1.3, height=1.3, legend_out=(True),margin_titles=True)
    grid.set(xlim=(-20, 20))
    grid.tight_layout()
    grid.map(sns.scatterplot, "bias","rmse", marker="o")
    
    plt.savefig('calibration_outputs/'+glacier+'_scatter_dv.pdf', format='pdf')
    
    df.plot()
    df=df[df.score > 5]
    #scoremax = 25
    n_model = 5
    print(df.nsmallest(n_model,'score'))
    
    #html = df.nsmallest(n_model,'score').to_html()
    
    #text_file = open('outputs/'+glacier+'_bestmodels_dv.html', "w")
    #text_file.write(html)
    #text_file.close()
    df.nsmallest(n_model,'score').to_csv('calibration_outputs/'+glacier+'_bestmodels_dv.csv')
    
    #best_dfs = best_dfs.append(df.nsmallest(n_model,'score'))
    #histo = best_dfs.groupby(['model']).count()['score']
    #histo = histo.append(best_dfs.groupby(['A']).count()['score'])
    #histo = histo.append(best_dfs.groupby(['C']).count()['score'])
   
    #plt.figure()
    #histo.sort_values().plot(kind = 'bar')
    #plt.tight_layout()
    #plt.savefig('outputs_plots/occurences_dv.svg', format='svg')
    
    
    
    
    
