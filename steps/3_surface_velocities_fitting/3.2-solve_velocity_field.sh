#!/bin/sh
#=========================================
# AUTHOR: Damien Maure
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  2021
# MODIFIED Vincent Peyaud, may 2022
#
# OBJECT : calls Elmer/Ice to solve the velocity field for every case described above.
# /!\ the process can be very long. Don't hesitate to change the mesh resolution for faster calculus.
#   The Navier-Stokes solver can sometimes diverge, in wich case more work is needed to change the linear and non-linear system solver accordingly.
#
#==========================================
HOME_DIR=$(pwd)


A_list=$HOME_DIR/supplementary_material/A_list.txt
C_list=$HOME_DIR/supplementary_material/C_list.txt
glacier_list=$HOME_DIR/../1_glacier_selection_and_merging/glacier_group_list.txt

# We start by making the output surface_speed directories
while read gl ;do
	mkdir outputs/$gl
done < $glacier_list


# Now we loop over desired A values for all gl groups and models
while read group;do
cd dirs/$group
while read A ;do
	while read C; do
	echo "Starting simulations for A =" $A "and C=" $C
	bash ../../supplementary_material/solver_steady.sh $A $C
sleep 3
echo
echo
	done < $C_list
done < $A_list
cd ../..
done < $glacier_list
