#=========================================
# AUTHOR: Damien Maure
# ORGANIZATION: IGE(CNRS-France)
# CREATED Created on Jul 2021
# MODIFIED Vincent Peyaud, may 2022
#
# OBJECT: converts the vtu files obtaines via Elmer/Ice to netcdf files in *outputs/* (An altenative is to use the SaveGridData solver developped by Fabien Gillet-Chaulet)
#=========================================

### Functions
function echoe { echo -e "\033[34m" $1  "\033[00m" ; }
function echor { echo -e "\033[31m" $1  "\033[00m" ; } # echo in Red

export HOME_DIR=$(pwd)

#echoe "aaa"
#echoe `pwd`
# Check if the directory outputs/ exists
if [ -d "outputs" ]
   then echo "Directory outputs exists"
   else echor "Create first directory outputs"
        exit 1
fi

A_file=$HOME_DIR/supplementary_material/A_list.txt
C_file=$HOME_DIR/supplementary_material/C_list.txt
cd dirs # in dirs/
echoe `pwd`
for glacier in *;do
    echoe "glacier $glacier"
	mkdir $HOME_DIR/outputs/$glacier
	cd $glacier # in dirs/$glacier/
	while read A; do
		mkdir $HOME_DIR/outputs/$glacier/$A
		cd $A # in dirs/$glacier/$A/
		while read C; do
			cd $C # in dirs/$glacier/$A/$C/
			mkdir $HOME_DIR/outputs/$glacier/$A/$C
			for model in *;do
			echoe "$model"
			cd $model # in dirs/$glacier/$A/$C/$model/

			#cat surf_datapar*.dat > surf_data.dat
            echoe "In $pwd"
            ls -rtl *txt
			python $HOME_DIR/supplementary_material/dat_to_nc.py
			cp surf_speed.nc $HOME_DIR/outputs/$glacier/$A/$C/${model}.nc

			cd ..
		done
		cd .. # in dirs/$glacier/$A/$C/
	done < $C_file
	cd .. # in dirs/$glacier/$A/
	done < $A_file
cd ..
done
