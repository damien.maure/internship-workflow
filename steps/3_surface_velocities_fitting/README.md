# Step 3 Surface_velocities_fitting

Creates the file tree needed for Elmer/Ice to calculate the velocities (INIT.sif and steady.sif). Then it compares modelled velocity with observed velocity. 


# FILES:

- **3.1-makedirs.sh** is a script that creates the file tree needed for Elmer/Ice to calculate the velocities on a given glacier group with **every** combination possible between *bedrock*, *friction coefficient value* and *creep factor* specified in respectively *supplementary_material/C_list.txt* and */A_list*.
By default, the mesh resolution is 100m. It can be changed trough the variable *res*.

- **3.2-solve_velocity_field.sh** calls Elmer/Ice to solve the velocity field for every case described above.
/!\ the process can be very long. Don't hesitate to change the mesh resolution for faster calculus. The Navier-Stokes solver can sometimes diverge, in wich case more work is needed to change the linear and non-linear system solver accordingly.

- **3.3-netcdf_conversion.sh** converts the vtu files obtaines via Elmer/Ice to netcdf files in *outputs/* (An altenative is to use the SaveGridData solver developped by Fabien Gillet-Chaulet)

- **3.4-best_parameterization_selection.py** uses the netcdf files of the step above to compute the RMSE of the surface velocity (using Dehecq. et al method) from satellite measurments, on every parameterization possible. The 5 best parameterization are then selected and written in *calibration_outputs/<glacier_gr_nb>_bestmodels_dv.csv*

- **outputs/** contains the converted results of step *3.3*

- **calibration_outputs/** contains the files created by step *3.4*

- **dirs/** contains the tree file used by step *3.2* to run Elmer/Ice across the different parameterizations.

- **supplementary_material/** contains all the necessary files for Elmer to run, along with subscripts called by the different steps. It is important to check sif files to control used solvers and particularly Stokes resolution method. More info is available in *supplementary_material/README.md*


# TO PROCESS THE STEP

0. create dirs/ 
```
mkdir dirs
```

1. run 
```
bash 3.1-makedirs.sh
```
You can change the resolution directly in the file.

2. Call Elmer/Ice by running

```
bash 3.2-solve_velocity_field.sh
```
**WARNING** this step can be very very long. Some work is needed to be able to apply it on very large glaciers.

3. extract the surface velocity field computed with
```
bash 3.3-netcdf_conversion.sh
```

4. eventually select the best performing couples with
```
pyhon 3.4-best_parameterization_selection.py
```

# AT THE END OF THIS STEP

3.1 for all group/model/A/C : dirs/group_nb/100/0.1/model/INPUT/ thick.nc DEMs.nc rectangle/
(thickness and surface DEM + a mesh)


# Things that need care or that can be changed

1. mesh resolution, in step 3.1

2. surface and thickness **CRS** in step 3.1 and 3.3, depending on the input surface dataset and the region modelled

3. Elmer Solvers (in *supplementary_material/*) need to be compiled with elmerf90.

4. the way of selecting the best combinations in 3.4

5. the *linear system solver* of Elmer .sif (in *supplementary_material/sif_contents*)
It is technical, but crutial. Iterative solvers don't often converge with sliding. So direct solvers are (from experience) more useful here. (cpardiso works well on dahu with 16 partitions for example)

6. For WAY faster computations, the sliding can be deactivated in the *.sif*.

