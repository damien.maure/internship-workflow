#=========================================
# AUTHOR: Damien Maure
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  2021
#
# OBJECT : creates the file tree needed for Elmer/Ice to calculate the velocities on a given glacier group with **every** combination possible between *bedrock*, *friction coefficient value* and *creep factor* specified in respectively *supplementary_material/C_list.txt* and */A_list*.
# By default, the mesh resolution is 100m. It can be changed in line 18; with the variable *res*.

#
#==========================================

### Variables
# This is the resolution of the mesh
res=100.0
# number of partitions (16 for dahu calculator)
npart=16 # 8

### Functions
function ncmin { ncap2 -O -C -v -s "foo=${1}.min();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function ncmax { ncap2 -O -C -v -s "foo=${1}.max();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }


### Script

# We create aliases to call files more easily
export HOME_DIR=$(pwd)
data_path=$HOME_DIR/../../data
bed_path=$HOME_DIR/../2_DEMs_merging
shp_path=$HOME_DIR/../../data

global_data_path=$data_path/surfDEMs/rgi_glob.tif

file='../1_glacier_selection_and_merging/glacier_group_list.txt'

# These are the files with the values of A and C we want to model
A_list=$HOME_DIR'/supplementary_material/A_list.txt'
C_list=$HOME_DIR'/supplementary_material/C_list.txt'


# Check if the directory dirs exists
if [ -d "dirs" ]
   then echo "Directory dirs exists"
   else echo "Create first directory dirs"
        exit 1
fi
# For every glacier group, we create a directory
while read line;do	# loop reading file
    echo -e "\033[34m ${line} \033[00m"
	mkdir dirs/$line
	cd dirs/
	cd $line
	while read A; do # loop reading A
	# in the group directory, create a subdirectory for every A value wanted
	mkdir $A
	cd $A
	while read C;do # loop reading C
	# in the A subdirectory, cretae a sub-subdirectory for every C value wanted
	mkdir $C
	cd $C

	# For every sub-subdirectory, we create a sub-sub-subdirectory for every thickness model we have (its over now i promise)
	for model_path in $data_path/thickDEMs/*;do	# loop for on models
		# we get the model name
		model=$(echo $model_path | rev | cut -d'/' -f 1| rev)
		# and we create the INPUT (where we will put meshes and DEMs)
		# and the OUTPUT (where Elmer will store the .vtu files)

		mkdir $model
		mkdir $model/INPUT
		mkdir $model/OUTPUT

		cd $model
  
        echo -e "\033[34m A C Model ${A}  ${C}  ${model} \033[00m"
        echo -e "\033[34m working in `pwd` \033[00m"
        
		#gdal_translate -of netcdf $data_path/surfDEMs/surface_DEM_$line.tif INPUT/surface.nc
		#gdal_translate -of netcdf $data_path/thickDEMs/${line}_thickness.tif INPUT/thick.nc


        # Import composite thickness
		cp $bed_path/DEMs/bedrock/$line/$model/merged.tif INPUT/thick.tif
		gdal_translate -of netcdf INPUT/thick.tif INPUT/thick.nc
        echo -e "\033[34m Created `ls -l INPUT/thick.nc`  \033[00m"

       #### Import composite surface (This was done previously with DEMs dataset for every RGI entity, but we now get the surface from a GLOBAL dataset)
####		#cp $data_path/surfDEMs/surface_DEMs_RGI60-11_grouped/$line/surface.nc INPUT/surface.nc
# Import global surface
		
		# This is the surface raster resolution in m
		cs=25
		# Get the extent of the thickness raster
		xmin0=$(ncmin x INPUT/thick.nc)
		xmax0=$(ncmax x INPUT/thick.nc)
		ymin0=$(ncmin y INPUT/thick.nc)
		ymax0=$(ncmax y INPUT/thick.nc)
		
		# Because of how gdal works, xmin0,xmax0... etc are the center of the boudary cells, so we need the boudary of the boundary cells to warp our surface data (need to add or subtract cs/2)

		xmin=$(echo "scale=10; $xmin0-$cs/2" | bc -l)
		ymin=$(echo "scale=10; $ymin0-$cs/2" | bc -l)
		xmax=$(echo "scale=10; $xmax0+$cs/2" | bc -l)
		ymax=$(echo "scale=10; $ymax0+$cs/2" | bc -l)

		echo -e "\033[34m Extent boudary $xmin $xmax $ymin $ymax \033[00m"
		

		# We now warp our surface GLOBAL dataset onto the thickness DEM.
		gdalwarp -ot Float32 -s_srs EPSG:4326 -t_srs EPSG:32632 -r bilinear -of GTiff -tr $cs $cs -te $xmin $ymin $xmax $ymax -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $global_data_path INPUT/surface.tif
		gdal_translate -of netcdf INPUT/surface.tif INPUT/surface.nc
        echo -e "\033[34m Created `ls -l INPUT/surface.nc`  \033[00m"
		######
		#gdal_translate -of netcdf $data_path/surfDEMs_glob/11-mtblanc.tif INPUT/toclip.nc
		#gdaltindex INPUT/clip.shp INPUT/thick.nc
		#gdalwarp  -r cubic -tr 25 -25 -cutline INPUT/clip.shp -crop_to_cutline INPUT/toclip.nc INPUT/surface.nc
		
		#gdalwarp -ot Float32 -t_srs EPSG:32633 -r cubic -of GTiff -tr 25 25 -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $HOME_DIR/Data/surfDEMs_glob/11-mtblanc.tif INPUT/toclip.tif
		#gdal_translate -of netcdf INPUT/toclip.tif INPUT/toclip.nc

		#gdalwarp -cutline INPUT/clip.shp -crop_to_cutline INPUT/toclip.tif INPUT/surface.tif
		######

		# We finally merge surface and thickness into a single DEMs.nc file with "bed"(=surface-thickness) and "surface" bands
	    ncrename -v Band1,surface INPUT/surface.nc
		ncrename -v Band1,thick  INPUT/thick.nc
		#gdal_merge.py -a_nodata -9999 -ot Float32 -separate -o INPUT/surface.nc -of GTif INPUT/surface.nc INPUT/thick.nc
		ncks -A INPUT/thick.nc INPUT/surface.nc
		ncap2 -s "bed=surface-thick" INPUT/surface.nc INPUT/DEMs.nc
        echo -e "\033[34m Created `ls -l INPUT/DEMs.nc`  \033[00m"

		
		# Get back the extent of the DEMs
		xmin=$(ncmin x INPUT/DEMs.nc)
		xmax=$(ncmax x INPUT/DEMs.nc)
		ymin=$(ncmin y INPUT/DEMs.nc)
		ymax=$(ncmax y INPUT/DEMs.nc)
		
		# This is the resolution of the mesh
		echo -e "\033[34m resolution is res= ${res}  \033[00m"

		echo -e "\033[34m mesh boundaries are $xmin $xmax $ymin $ymax  \033[00m"
		
		# we take an universal rectangular .geo file that needs x/y extents to be specified
		cp $HOME_DIR/supplementary_material/rectangle.geo INPUT/rectangle.geo

		# and now we specify the resolution and boundaries we want to gmsh
        echo -e "\033[34m echo gmsh INPUT/rectangle.geo -> INPUT/rectangle.msh  \033[00m"
		gmsh -1 -2 INPUT/rectangle.geo -setnumber xmin $xmin -setnumber xmax $xmax \
					 -setnumber ymin $ymin -setnumber ymax $ymax \
					 -setnumber lc $res 
		

        echo -e "\033[34m ElmerGrid \033[00m"
		# Elmer grid converts it to a format Elmer understands
		ElmerGrid 14 2 INPUT/rectangle.msh -autoclean
		ElmerGrid 2 5 INPUT/rectangle # this is for paraview visualisation, not really needed

		## HERE IS WHERE WE CHOOSE THE NUMBER OF PARTITIONS WANTED. (here 8, can be changed for dahu runs to 16)
		ElmerGrid 14 2 INPUT/rectangle.msh -metis $npart -autoclean
		cd ..
		
#fi
    done     # loop for on models
    cd ..
    done < $C_list # loop reading C
	cd ..
    done < $A_list # loop reading A
	cd $HOME_DIR
done < $file # loop reading file

