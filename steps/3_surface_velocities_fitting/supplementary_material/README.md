# Supplementary material (step 3)

Files needed in Step 3 to Run Elmer_Solver
(.sif, .F90 etc) 

# FILES:

**sif_contents/** is a directory containing everything thats needed for Elmer to run (.sif files, solvers and .lua files)
    INIT.sif   : Create 3D mesh of the glacier (.vtu and .result)
    steady.sif : Restart the previous result and run to a obtain a steady state 


**A_list.txt** is a list of A values that we want to test

**C_list.txt** is a list of C values that we want to test. ***warning:*** Given the workflow, EVERY combination possible is tested (nb_bedrocks*nb_A*nb_C). If more values are wanted, try a Latin hypercube or other ways of selecting combinations.

**dat_to_nc.py** is called by dat_to_nc.sh on every directory to convert .vtu files to .nc

**IcyMaskSolver(.F90)** is a solver used by Elmer. Needs to be compiled with elmerf90...

**SyntSMB(.F90)** is also a solver used by Elmer. Its not used in this step but needs to be compiled anyway.

**solver_steady.sh** is called by the step *3.2* to run Elmer on every A/C/model subdirectories. It has to be adapted for dahu jobs.

