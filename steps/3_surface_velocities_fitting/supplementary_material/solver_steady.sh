#=========================================
# AUTHOR: Damien Maure
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  2021
# MODIFIED: Vincent Peyaud May 2022
#
# OBJECT: This script is called by 3.2-solve_velocity_field.sh with 2 parameters $A $C. It change directory to dirs/$A/$C/
# Loop on $model: for each model copy supplementary_material/sif_contents/* and run INIT.SIF and steady.sif
#
#==========================================

### Functions
function ncdx { ncap2 -O -C -v -s "foo=(${1}.max()-${1}.min())/(${1}.size()-1);print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function echoe { echo -e "\033[34m" $1  "\033[00m" ; }
function echor { echo -e "\033[34m" $1  "\033[00m" ; }

### Script
echo $dx $dy

#number of partitions (16 for dahu calculator)
npart=16

export HOME_DIR=$(pwd)



file='src/~A-c-fit/glaciergrouplist.txt' #
cd $1
cd $2
echoe "cd $1 $2"
	for model in *; do
        echoe "cp supplementary_material/sif_contents -> ${model}"
		cp -RT  ../../../../supplementary_material/sif_contents $model
        #cp -RTv ../../../../supplementary_material/sif_contents $model # Verbosity added to check
		cd $model
        echoe "In $(pwd) "
		sed -e "s/<A>/$1/g;s/<C>/$2/g;" ../../../../../supplementary_material/sif_contents/Parameters.lua > Parameters.lua

		### ElmerSolver INIT.sif
        ### Adapt command in function of the computing infrastructure
        echor "mpirun -np ${npart} ElmerSolver INIT.sif"
		#mpirun -np 8 ElmerSolver INIT.sif # Launch directly
        #./script.sh...
        #oarsub -S ./RUN_Gricad_INIT_may22.sh
        ### ! Exemple for Dahu (Gricad)
            chmod u-x RUN_Gricad_INIT_may22.sh
            job_id=`oarsub -S ./RUN_Gricad_INIT_may22.sh  |grep OAR_JOB_ID|awk -F'=' '{print $2}'`
            echor $job_id
        
		dx=$(ncdx x INPUT/surface.nc)
		echoe "Set cell size to (ncdx x INPUT/surface.nc)" $dx
		sed -e "s/<cell_size>/$dx/g;" steady.sif > tmp.sif
		rm steady.sif
		mv tmp.sif steady.sif

		### ElmerSolver steady.sif
        ### Adapt command in function of the computing infrastructure
        echo "mpirun -np ${npart} ElmerSolver steady.sif"
		#mpirun -np 8 ElmerSolver steady.sif
        #./script.sh...
		cd ..
	done
cd $HOME_DIR
	

