--!#############################################################
--! A list of usefull constants for glacier simulations
--!  - Unit System =
--!     -- m
--!     -- a
--!     -- MPa
--!#############################################################

--!## Convertion factor year to seconds
yearinsec = 365.25*24*60*60

--!## Ice density
rhoi = 910.0/(1.0e6*yearinsec^2)   

--!## Gravity
gravity = -9.81*yearinsec^2

--!## Minimum critical thickness
MinH=1.0

--!## Flow Law
-- rate factor
A=<A>
-- Weertman coeff
C=<C>
-- Glen exponent
n=3.0
