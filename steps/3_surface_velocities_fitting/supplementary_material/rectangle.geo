// Generic gmsh .geo file to mesh a rectangular domain
// parameters: resolution and rectangle length and width
//  (can be changed in line using e.g. "-setnumber lc [VALUE]")
DefineConstant[ lc = {0.1, Name "resolution"},
                xmin = {0.0, Name "xmin"},
		xmax = {1.0, Name "xmax"},
		ymin = {0.0, Name "ymin"},
                ymax = {1.0, Name "ymax"}];

////////////////////////////////////////////////////////
Mesh.Algorithm=5;
// Points
Point(1) = {xmin, ymin, 0, lc};
Point(2) = {xmax, ymin, 0, lc};
Point(3) = {xmax, ymax, 0, lc};
Point(4) = {xmin, ymax, 0, lc};
// Lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
// Surface
Curve Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};

// Physical 
Physical Line(1) = {1,2,3,4};
//+
Physical Surface(2) = {1};
