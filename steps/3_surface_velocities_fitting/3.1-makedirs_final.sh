#=========================================
# AUTHOR: Damien Maure
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  2021
# MODIFIED Vincent Peyaud, may 2022
#
# OBJECT : creates the file tree needed for Elmer/Ice to calculate the velocities on a given glacier group with **every** combination possible between *bedrock*, *friction coefficient value* and *creep factor* specified in respectively *supplementary_material/C_list.txt* and */A_list*.
# By default, the mesh resolution is 100m. It can be changed in line 18; with the variable *res*.

#
#==========================================

### Variables
# This is the resolution of the mesh
res=100.0
# number of partitions (16 for dahu calculator)
npart=16 # 8

# directory to store create and the meshes
meshes=valid_models
#surface raster resolution in m
cs=25

# !!! Projection : be careful with projection using gdalwarp


### Functions
function ncmin { ncap2 -O -C -v -s "foo=${1}.min();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function ncmax { ncap2 -O -C -v -s "foo=${1}.max();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function echoe { echo -e "\033[34m" $1  "\033[00m" ; } # echo in Blue
function echor { echo -e "\033[31m" $1  "\033[00m" ; } # echo in Red

### Script


# Aliases to call files more easily
export HOME_DIR=$(pwd)
data_path=$HOME_DIR/../../data
bed_path=$HOME_DIR/../2_DEMs_merging
shp_path=$HOME_DIR/../../data

global_data_path=$data_path/surfDEMs/rgi_glob.tif

file='../1_glacier_selection_and_merging/glacier_group_list.txt'

# These are the files with the values of A and C we want to model
A_list=$HOME_DIR'/supplementary_material/A_list.txt' # Viscosity coefficient
C_list=$HOME_DIR'/supplementary_material/C_list.txt' # Sliding coefficient


# Check if the directory dirs exists
if [ -d "dirs" ]
   then echo "Directory dirs exists"
   else echor "Create first directory dirs"
        exit 1
fi

### First substep: For every glacier group, we create a directory and a mesh
while read line;do	# loop reading file
    echoe "Glacier group nb ${line}"
	mkdir dirs/$line
	cd dirs/
	cd $line # in dirs/$line

    mkdir ${meshes} # Create a directory to store meshes for all model.
    cd ${meshes} # in dirs/$line/${meshes}
    
	# For every sub-subdirectory, we create a sub-sub-subdirectory for every thickness model we have (its over now i promise)
	for model_path in $data_path/thickDEMs/*;do	# first loop for on models
		# we get the model name
		model=$(echo $model_path | rev | cut -d'/' -f 1| rev)
		# and we create the INPUT (where we will put meshes and DEMs)
		# and the OUTPUT (where Elmer will store the .vtu files)
        
        # Test if the group/model is valid (i.e. there is a thick file)
        thickNcFileName=$bed_path/DEMs/bedrock/$line/$model/thick.nc
        echo "looking for $thickNcFileName"
        if [ ! -f $thickNcFileName ] # if not valid
        then
            echor "not thickness DEM for $line $model: model excluded! \n"          # do nothing, just a warning
        
        else  #if valid: create a directory model, copy thick, surf DEMs and create a mesh
        
		    mkdir $model # create a directory model
	        mkdir $model/INPUT $model/OUTPUT

            cd $model #### in dirs/$line/${meshes}/$model
  
            #echoe "A C Model ${A}  ${C}  ${model}"
            echoe "OK working in `pwd`"
        
            		#gdal_translate -of netcdf $data_path/surfDEMs/surface_DEM_$line.tif INPUT/surface.nc
            		#gdal_translate -of netcdf $data_path/thickDEMs/${line}_thickness.tif INPUT/thick.nc


            # Import composite thickness in a NC file
            cp $bed_path/DEMs/bedrock/$line/$model/merged.tif INPUT/thick.tif
            gdal_translate -of netcdf INPUT/thick.tif INPUT/thick.nc
            echoe "Created `ls INPUT/thick.nc`"


            # Import global surface: use a GLOBAL dataset and extract inside the extent of the tickness DEM
            # reminder surface dataset path is $global_data_path
            
            # This is the surface raster resolution in m
            cs=25
            # Get First the extent of the thickness raster
            xmin0=$(ncmin x INPUT/thick.nc)
            xmax0=$(ncmax x INPUT/thick.nc)
            ymin0=$(ncmin y INPUT/thick.nc)
            ymax0=$(ncmax y INPUT/thick.nc)
            # Because of how gdal works, xmin0,xmax0... etc are the center of the boudary cells, so we need the boudary of the boundary cells to warp our surface data (need to add or subtract cs/2)
            xmin=$(echo "scale=10; $xmin0-$cs/2" | bc -l)
            ymin=$(echo "scale=10; $ymin0-$cs/2" | bc -l)
            xmax=$(echo "scale=10; $xmax0+$cs/2" | bc -l)
            ymax=$(echo "scale=10; $ymax0+$cs/2" | bc -l)

            printf "Extent boudary %.2f %.2f %.2f %.2f \n" $xmin $xmax $ymin $ymax
            #echoe "Extent boudary $xmin $xmax $ymin $ymax "
		
            # We now warp our surface GLOBAL dataset onto the thickness DEM.
            gdalwarp -ot Float32 -s_srs EPSG:4326 -t_srs EPSG:32632 -r bilinear -of GTiff -tr $cs $cs -te $xmin $ymin $xmax $ymax -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $global_data_path INPUT/surface.tif
            gdal_translate -of netcdf INPUT/surface.tif INPUT/surface.nc
            echoe "Created `ls INPUT/surface.nc`"

            # We finally merge surface and thickness into a single DEMs.nc file with "bed"(=surface-thickness) and "surface" bands
            ncrename -v Band1,surface INPUT/surface.nc
            ncrename -v Band1,thick  INPUT/thick.nc
            #gdal_merge.py -a_nodata -9999 -ot Float32 -separate -o INPUT/surface.nc -of GTif INPUT/surface.nc INPUT/thick.nc
            ncks -A INPUT/thick.nc INPUT/surface.nc
            ncap2 -s "bed=surface-thick" INPUT/surface.nc INPUT/DEMs.nc
                    echoe "Created bedrock `ls INPUT/DEMs.nc`"

		
            # Get back the extent of the DEMs
            xmin=$(ncmin x INPUT/DEMs.nc)
            xmax=$(ncmax x INPUT/DEMs.nc)
            ymin=$(ncmin y INPUT/DEMs.nc)
            ymax=$(ncmax y INPUT/DEMs.nc)
		
            # This is the resolution of the mesh
            echoe "resolution is res= ${res} "

            echoe "mesh boundaries are $xmin $xmax $ymin $ymax"
		
            # we take an universal rectangular .geo file that needs x/y extents to be specified
            cp $HOME_DIR/supplementary_material/rectangle.geo INPUT/rectangle.geo

            # and now we specify the resolution and boundaries we want to gmsh
            echoe "echo gmsh INPUT/rectangle.geo -> INPUT/rectangle.msh"
     		gmsh -1 -2 INPUT/rectangle.geo -setnumber xmin $xmin -setnumber xmax $xmax \
					 -setnumber ymin $ymin -setnumber ymax $ymax \
					 -setnumber lc $res 
		

            echoe "ElmerGrid"
            # Elmer grid converts it to a format Elmer understands
    		ElmerGrid 14 2 INPUT/rectangle.msh -autoclean
    		ElmerGrid 2 5 INPUT/rectangle # this is for paraview visualisation, not really needed

		    # HERE IS WHERE WE CHOOSE THE NUMBER OF PARTITIONS WANTED. (specified in the header, here 16)
		    ElmerGrid 14 2 INPUT/rectangle.msh -metis $npart -autoclean

            cd .. # Up to dirs/$line/${meshes}
            # pwd # check the path


        fi # Test if the group/model is valid
    
    done # first loop for on models
    cd .. # Up to dirs/$line


    echoe "\n create directories for A & C  and copy the mesh: \n"

### Second step: copy meshes into every directory (with several A and C tested)

    while read A; do # loop reading A
        # in the group directory, create a subdirectory for every A value wanted
        mkdir ./$A
    while read C;do # loop reading C
        # in the A subdirectory, cretae a sub-subdirectory for every C value wanted
        mkdir ./$A/$C
                    
        echoe " copy ${meshes} into ./$A/$C/"
        
        cp -r ${meshes}/* ./$A/$C/
        

    done < $C_list # loop reading C
    done < $A_list # loop reading A


done < $file # loop reading file
