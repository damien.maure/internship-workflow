#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-06
# MODIFIED: 
#  
#
#========================================== 
############################
"""
Created on Tue Jul  6 11:09:42 2021

@author: maured
"""

from oggm import cfg, utils, workflow, tasks, graphics, global_tasks, shop
from oggm.core import flowline, climate, massbalance, gcm_climate
import geopandas as gpd
#import salem
import xarray as xr
#import pandas as pd
#import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import os


#SPINUP PARAM
nb_years_run = 82
nb_years_spinup = 100 # in years
z_min = 0
z_max = 4900

#centeryear = 2015
halfsize = 10

url = 'https://cluster.klima.uni-bremen.de/~oggm/gdirs/oggm_v1.4/L3-L5_files/CRU/elev_bands/qc3/pcp2.5/no_match/'
cfg.initialize(logging_level='WARNING') 
cfg.PATHS['working_dir'] = 'oggm_files'
cfg.PARAMS['border'] = 80


f = open('../1_glacier_selection_and_merging/glacier_group_list.txt',"r")
groups = f.readlines()
f.close()
rgi_11 = gpd.read_file('../1_glacier_selection_and_merging/rgi_grouped/rgi.shp')

for group in groups:
    group = group[:-1]
    group_glaciers = rgi_11[rgi_11.glacier_gr == int(group)]
    area_tot = sum(group_glaciers.Area.values)
    
    fin_vol = {'rcp26':np.zeros([1,nb_years_run]),'rcp45':np.zeros([1,nb_years_run]),'rcp60':np.zeros([1,nb_years_run]),'rcp85':np.zeros([1,nb_years_run])}
    fin_area = {'rcp26':np.zeros([1,nb_years_run]),'rcp45':np.zeros([1,nb_years_run]),'rcp60':np.zeros([1,nb_years_run]),'rcp85':np.zeros([1,nb_years_run])}
    
    heights = np.arange(z_min,z_max)
    
    group_mb = {'rcp26':np.zeros([len(heights),nb_years_run]),'rcp45':np.zeros([len(heights),nb_years_run]),'rcp60':np.zeros([len(heights),nb_years_run]),'rcp85':np.zeros([len(heights),nb_years_run])}
    for idx,glacier in group_glaciers.iterrows():
        
        pond = glacier.Area/area_tot
        
        
#OGGM RUNS
        gdirs = workflow.init_glacier_regions([glacier.RGIId], from_prepro_level=3, prepro_base_url=url)
        gdir = gdirs[0]
        
        
        
        tasks.run_random_climate(gdir, seed = 1, nyears = nb_years_spinup-1, y0=None, halfsize = halfsize, store_model_geometry=True, output_filesuffix='spinup')
        
        
        
        bp = 'https://cluster.klima.uni-bremen.de/~oggm/cmip5-ng/pr/pr_mon_IPSL-CM5A-LR_{}_r1i1p1_g025.nc'
        bt = 'https://cluster.klima.uni-bremen.de/~oggm/cmip5-ng/tas/tas_mon_IPSL-CM5A-LR_{}_r1i1p1_g025.nc'
        for rcp in ['rcp26', 'rcp45', 'rcp60', 'rcp85']:
            # Download the files
            ft = utils.file_downloader(bt.format(rcp))
            fp = utils.file_downloader(bp.format(rcp))
            # bias correct them
            workflow.execute_entity_task(gcm_climate.process_cmip_data, gdirs, 
                                         filesuffix='_IPSL-CM5A-LR_{}'.format(rcp),  # recognize the climate file for later
                                         fpath_temp=ft,  # temperature projections
                                 fpath_precip=fp,  # precip projections
                                 );
            
        for rcp in ['rcp26', 'rcp45', 'rcp60', 'rcp85']:
            rid = '_IPSL-CM5A-LR_{}'.format(rcp)
            workflow.execute_entity_task(tasks.run_from_climate_data, gdirs, ys=2020, 
                                 climate_filename='gcm_data',  # use gcm_data, not climate_historical
                                 climate_input_filesuffix=rid,  # use the chosen scenario
                                 init_model_filesuffix='spinup',  # this is important! Start from 2020 glacier
                                 output_filesuffix=rid,  # recognize the run for later
                                );
            
            mbmod = massbalance.MultipleFlowlineMassBalance(gdir, mb_model_class=massbalance.PastMassBalance,
                                     bias=None,
                                     filename='gcm_data',
                                     input_filesuffix=rid)
            mbmod1 = mbmod.flowline_mb_models[0]
            mb_array= np.zeros((len(heights),nb_years_run))
            i=0
            for year in range(2020, 2100):
               mb = mbmod1.get_annual_mb(heights,year=year) * cfg.SEC_IN_YEAR * 10/9#* cfg.PARAMS['ice_density']
               mb_array[:,i] = mb
               i+=1
            group_mb[rcp]+= pond * mb_array
               
            
        
        
            
        f, ax1 = plt.subplots(1, 1, figsize=(7, 4))
        for rcp in ['rcp26', 'rcp45', 'rcp60', 'rcp85']:
            rid = '_IPSL-CM5A-LR_{}'.format(rcp)
            ds = utils.compile_run_output(gdirs, input_filesuffix=rid)
            ds.isel(rgi_id=0).volume.plot(ax=ax1, label=rcp);
            fin_vol[rcp]+=  ds.isel(rgi_id=0).volume.values
            fin_area[rcp]+=  ds.isel(rgi_id=0).area.values
            
            
            
        
            
        plt.legend();
        plt.show()
    plt.figure()
    
    for rcp in ['rcp26', 'rcp45', 'rcp60', 'rcp85']:
        np.savetxt('outputs/'+str(group)+'/1D_var_'+rcp+'.csv', np.vstack([fin_vol[rcp].flatten(),fin_area[rcp].flatten()]), delimiter=",")
        plt.plot(fin_vol[rcp].flatten())
    plt.show()
    
    for rcp in ['rcp26', 'rcp45', 'rcp60', 'rcp85']:
        nc = xr.DataArray(group_mb[rcp],dims=['zco','tco'], name='mb')
        nc = nc.assign_coords({'zco':heights, 'tco':np.arange(0,nb_years_run)})
        
        z = xr.DataArray(heights,dims=['zco'],name = 'z')
     
        t = xr.DataArray(np.arange(0,nb_years_run),dims=['tco'],name = 't')
        
        nc = nc.to_dataset(name='mb')
        nc['z'] = z
        nc['t'] = t
        nc.to_netcdf('outputs/'+group+'/smbs/smb_'+rcp+'.nc')
    
        
        
        

        
