import pandas as pd
import os


path_to_csv = '../3_surface_velocities_fitting/calibration_outputs'

f = open('../1_glacier_selection_and_merging/glacier_group_list.txt',"r")
grouplist = f.readlines()
f.close()

dict_models = {'Millan':'r_model','Composite':'composite','Farinotti':'results_model_1','Frey':'results_model_2','Maussion':'results_model_3','Fürst':'results_model_4'}

for group in grouplist:
    group = group[:-1]
    dirs = pd.read_csv(path_to_csv + group + '_bestmodels_dv.csv')
    try:
        os.mkdir('dirs/'+group)
    except:
        print('file exist')
    for i, case in dirs.iterrows():
        model = case['model']
        dir_str = dict_models[model]+ '+' + str(case['C'])+ '+' + str(case['A'])
        os.mkdir('dirs/'+group+'/'+dir_str)
        
    
    
    
