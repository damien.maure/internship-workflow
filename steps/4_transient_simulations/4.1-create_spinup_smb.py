#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-06
# MODIFIED: 
#  
#
#========================================== 
############################

"""
Created on Tue Jun 15 08:27:03 2021

@author: maured
"""

from oggm import cfg, utils, workflow, tasks, graphics, global_tasks, shop
from oggm.core import flowline, climate, massbalance
import geopandas as gpd
#import salem
import xarray as xr
#import pandas as pd
#import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import os



nb_years_run = 100 # in years
z_min = 0
z_max = 4900

#centeryear = 2015
halfsize = 10

url = 'https://cluster.klima.uni-bremen.de/~oggm/gdirs/oggm_v1.4/L3-L5_files/CRU/elev_bands/qc3/pcp2.5/no_match/'
cfg.initialize(logging_level='WARNING') 
cfg.PATHS['working_dir'] = 'oggm_files'
cfg.PARAMS['border'] = 80


f = open('../1_glacier_selection_and_merging/glacier_group_list.txt',"r")
groups = f.readlines()
f.close()
rgi_11 = gpd.read_file('../1_glacier_selection_and_merging/rgi_grouped/rgi.shp')

for group in groups:
    group = group[:-1]
    group_glaciers = rgi_11[rgi_11.glacier_gr == int(group)]
    area_tot = sum(group_glaciers.Area.values)
    
    fin_vol = np.zeros([1,nb_years_run])
    fin_area = np.zeros([1,nb_years_run])
    
    heights = np.arange(z_min,z_max)
    
    group_mb = np.zeros((len(heights),nb_years_run))
    for idx,glacier in group_glaciers.iterrows():
        
        pond = glacier.Area/area_tot
        
        
#OGGM RUNS
        gdirs = workflow.init_glacier_regions([glacier.RGIId], from_prepro_level=3, prepro_base_url=url)
        gdir = gdirs[0]
        
        
        
        tasks.run_random_climate(gdir, seed = 1, nyears = nb_years_run-1, y0=None, halfsize = halfsize, store_model_geometry=True)#,output_filesuffix='_spinup')
    
    
        #fmod = flowline.FileModel(gdir.get_filepath('model_geometry'))
        #fmod.run_until(nb_years_run)
        #graphics.plot_modeloutput_map(gdir, model=fmod)
    
        results = xr.open_dataset(gdirs[0].get_filepath('model_geometry'))
        
        fin_vol += results.volume_m3.values
        fin_area += results.area_m2.values
        results.close()
        
        
        
        
        # Method 1
        mbmod = massbalance.MultipleFlowlineMassBalance(gdir, mb_model_class=massbalance.RandomMassBalance,
                                     y0=None, halfsize=halfsize,
                                     bias=None, seed=1,
                                     filename='climate_historical',
                                     input_filesuffix=None,
                                     unique_samples=None)
        mbmod1 = mbmod.flowline_mb_models[0]
        mb_array= np.zeros((len(heights),nb_years_run))
        for year in range(nb_years_run):
            mb = mbmod1.get_annual_mb(heights,year=year)* cfg.SEC_IN_YEAR * 10/9#* cfg.PARAMS['ice_density']
            mb_array[:,year] = mb
        group_mb+= pond * mb_array
     
    try:
        os.mkdir('outputs/'+group)
        os.mkdir('outputs/'+group+'/smbs')
    except:
        print()
            
        
        
        
        
        
        
        
        
        
    nc = xr.DataArray(group_mb,dims=['zco','tco'], name='mb')
    nc = nc.assign_coords({'zco':heights, 'tco':np.arange(0,nb_years_run)})
    
    z = xr.DataArray(heights,dims=['zco'],name = 'z')
 
    t = xr.DataArray(np.arange(0,nb_years_run),dims=['tco'],name = 't')
    
    nc = nc.to_dataset(name='mb')
    nc['z'] = z
    nc['t'] = t
    
    
    
    #nc = nc.assign(z=heights)
    #nc = nc.assign(t=np.arange(0,nb_years_run))
    
    
    
    
    
    
    nc.to_netcdf('outputs/'+group+'/smbs/spinup_smb.nc')
    
    try:
         t=np.arange(len(results.time))
    
         np.savetxt('outputs/'+str(group)+'/1D_var_spinup.csv', np.vstack([t,fin_vol.flatten(),fin_area.flatten()]), delimiter=",")
    except:
         print('again')
    #plt.figure()
    #plt.plot(results.time,fin_vol.flatten())
    #plt.show()
        
