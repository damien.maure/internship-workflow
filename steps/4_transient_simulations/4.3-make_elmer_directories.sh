#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-06
# MODIFIED: 
#  
#
#========================================== 
############################

function ncmin { ncap2 -O -C -v -s "foo=${1}.min();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }
function ncmax { ncap2 -O -C -v -s "foo=${1}.max();print(foo)" ${2} ~/foo.nc | cut -f 3- -d ' ' ; }

export HOME_DIR=$(pwd)
data_path=$HOME_DIR/../../data
bed_path=$HOME_DIR/../2_DEMs_merging
shp_path=$HOME_DIR/../../data

global_data_path=$data_path/surfDEMs/rgi_glob.tif
file='../1_glacier_selection_and_merging/glacier_group_list.txt'

python ../4.3-make_paths.py
while read line;do	
	echo "$line"
	cd dirs/
	cd $line
	for model_path in *;do
		echo $model_path	
		model=$(echo $model_path | cut -d'+' -f 1)
		#mkdir $model
		mkdir $model_path/INPUT
		mkdir $model_path/OUTPUT

		cd $model_path
		#gdal_translate -of netcdf $data_path/surfDEMs/surface_DEM_$line.tif INPUT/surface.nc
		#gdal_translate -of netcdf $data_path/thickDEMs/${line}_thickness.tif INPUT/thick.nc
#import composite thickness
		cp $bed_path/DEMs/bedrock/$line/$model/merged.tif INPUT/thick.tif
		gdal_translate -of netcdf INPUT/thick.tif INPUT/thick.nc
#import composite surface
		#cp $data_path/surfDEMs/surface_DEMs_RGI60-11_grouped/$line/surface.nc INPUT/surface.nc
#import global surface
		
		cs=25
		#number of pixel wanted around shapefile of glaceir
		n_margin=80

		xmin0=$(ncmin x INPUT/thick.nc)
		xmax0=$(ncmax x INPUT/thick.nc)
		ymin0=$(ncmin y INPUT/thick.nc)
		ymax0=$(ncmax y INPUT/thick.nc)
		
		xmin=$(echo "scale=10; ($xmin0-$cs/2-$cs*$n_margin)" | bc -l)
		ymin=$(echo "scale=10; ($ymin0-$cs/2-$cs*$n_margin)" | bc -l)
		xmax=$(echo "scale=10; ($xmax0+$cs/2+$cs*$n_margin)" | bc -l)
		ymax=$(echo "scale=10; ($ymax0+$cs/2+$cs*$n_margin)" | bc -l)

		echo $xmin $xmax $ymin $ymax

		mv INPUT/thick.tif INPUT/old_thick.tif
		rm INPUT/thick.tif

		gdalwarp -te $xmin $ymin $xmax $ymax INPUT/old_thick.tif INPUT/thick.tif
		gdal_translate -of netcdf INPUT/thick.tif INPUT/thick.nc

		gdalwarp -ot Float32 -s_srs EPSG:4326 -t_srs EPSG:32632 -r bilinear -of GTiff -tr $cs $cs -te $xmin $ymin $xmax $ymax -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $global_data_path INPUT/surface.tif
		gdal_translate -of netcdf INPUT/surface.tif INPUT/surface.nc
		#gdal_translate -of netcdf $data_path/surfDEMs_glob/11-mtblanc.tif INPUT/toclip.nc
		#gdaltindex INPUT/clip.shp INPUT/thick.nc
		#gdalwarp  -r cubic -tr 25 -25 -cutline INPUT/clip.shp -crop_to_cutline INPUT/toclip.nc INPUT/surface.nc
		
		#gdalwarp -ot Float32 -t_srs EPSG:32633 -r cubic -of GTiff -tr 25 25 -co COMPRESS=DEFLATE -co PREDICTOR=1 -co ZLEVEL=6 -wo OPTIMIZE_SIZE=TRUE $HOME_DIR/Data/surfDEMs_glob/11-mtblanc.tif INPUT/toclip.tif
		#gdal_translate -of netcdf INPUT/toclip.tif INPUT/toclip.nc

		#gdalwarp -cutline INPUT/clip.shp -crop_to_cutline INPUT/toclip.tif INPUT/surface.tif

	    	ncrename -v Band1,surface INPUT/surface.nc
		ncrename -v Band1,thick  INPUT/thick.nc
		#gdal_merge.py -a_nodata -9999 -ot Float32 -separate -o INPUT/surface.nc -of GTif INPUT/surface.nc INPUT/thick.nc
		ncks -A INPUT/thick.nc INPUT/surface.nc
		ncap2 -s "bed=surface-thick" INPUT/surface.nc INPUT/DEMs.nc


		

		xmin=$(ncmin x INPUT/DEMs.nc)
		xmax=$(ncmax x INPUT/DEMs.nc)
		ymin=$(ncmin y INPUT/DEMs.nc)
		ymax=$(ncmax y INPUT/DEMs.nc)

		res=100.0

		echo $xmin $xmax $ymin $ymax
		
		cp $HOME_DIR/src/1-preprocess/rectangle.geo INPUT/rectangle.geo

		gmsh -1 -2 INPUT/rectangle.geo -setnumber xmin $xmin -setnumber xmax $xmax \
					 -setnumber ymin $ymin -setnumber ymax $ymax \
					 -setnumber lc $res 

		ElmerGrid 14 2 INPUT/rectangle.msh -autoclean
		ElmerGrid 14 2 INPUT/rectangle.msh -metis 16 -autoclean
		ElmerGrid 2 5 INPUT/rectangle
		
		#Get the SMB file from OGGM
		cp $HOME_DIR/outputs/$line/smbs/smb_rcp85.nc smb.nc
		cp $HOME_DIR/outputs/$line/SMB.lua SMB.lua
		cd ..
	done
	cd $HOME_DIR
done < $file

