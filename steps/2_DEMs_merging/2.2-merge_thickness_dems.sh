#=========================================
# AUTHOR: Damien Maure
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  2021
# MODIFIED: Vincent Peyaud may 2022
#
# OBJECT : merges the files of every glacier group directory.
#
#==========================================
function echoe { echo -e "\033[34m" $1  "\033[00m" ; }
function echor { echo -e "\033[31m" $1  "\033[00m" ; }

echoe 'cd DEMs/bedrock'
cd DEMs/bedrock

#For every group and every thickness model we call gdal to merge all the .tif present in the corresponding directory
	for group in *;do
        echoe 'cd '$group
		cd $group
		for model in *;do
            echoe 'cd '$model
			cd $model
            
            TIFfile=`ls RGI*.tif` #files=(*) ; TIFfile=${files[0]}
            if [[ ! $TIFfile ]]; then
                 echor "in $model File not found!"
            else
			     # Here we merge every .tif file of the directory
			     gdal_merge.py -o merged.tif -q -n 0 $TIFfile #${files[0]}#`ls *.tif`
			     #and we translate it to netcdf format
			     gdal_translate -of netcdf merged.tif thick.nc
            fi
			cd ..
			done
		cd ..
		done

# At the end, every group-model has a 'thick.nc' file that will be used by Elmer later 

