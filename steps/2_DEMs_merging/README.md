# Step 2 Import and process the necessary DEMs for the simulation

### Thickness inversion data
We will use the dataset of inverted glacier thickness from [Farinotti & al. 2019](https://doi.org/10.1038/s41561-019-0300-3) as a start for our simulation. Different thickness estimates were calculated using a set of 5 inversion models. In addition, we will use the thickness inversion of [Millan & al.]().
We will use the composite rasters (average of different inversion models) for our simulations (see in /data)

### Surface DEMs
For the tutorial, a composite of tiles from ASTER Global Digital Elevation Model V003 will be used, with a resolution of 30mx30m. This dataset along with others can be downloaded from [NASA's EARTHDATA](https://search.earthdata.nasa.gov/search/) website.

*** COORDINATE SYSTEM WARNING ***

The workflow was designed to work on a glacier subset of the Alps. Therefore, the provided surface DEM and scripts are only valid given the CRS *ESPG32632*; Please take care to change this to the reference CRS used by Farinotti et al. in the region considered.

###FILES:

- **2.1-import_dems.py** creates a folder for every group of step 1 *glacier_group_list.txt*. It then imports every thickness DEM from the given group (from ~/data) in its folder.

- **2.2-merge_thickness_dems.sh** merges the files of every glacier group directory.
    Indicates if RGI*.tif file do not exist.

- **DEMs/** contains all the material used and created by the scripts above

# TO PROCESS THE STEP

0. Preparation
```
mkdir DEMs
Prepare/Check ../1_glacier_selection_and_merging/glacier_group_list.txt
```

1. run
```
python 2.1-import_dems.py
```

2. run
```
bash 2.2-merge_thickness_dems.sh 
```

# AT THE END OF THIS STEP

For all groups selected in ../1_glacier_selection_and_merging/glacier_group_list.txt : 
DEMs/bedrock/group_nb/model/ merged.tif & thick.nc

# Things that need care 

Empty group/model couples should be removed before step 3. 
