#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR: Damien Maure
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  2021
#
# OBJECT : creates a folder for every group of step 1 *glacier_group_list.txt*. It then imports every thickness DEM from the given group (from ~/data) in its folder.
#
#==========================================

import os
import geopandas as gpd

# Get the files from step 1
grouplist = '../1_glacier_selection_and_merging/glacier_group_list.txt'
shp_file = '../1_glacier_selection_and_merging/rgi_grouped/rgi.shp'
shp_file = '../1_glacier_selection_and_merging/rgi_grouped/rgi_MontBlanc_corrected.shp'
shp = gpd.read_file(shp_file)
groups = open(grouplist).read().splitlines()

# Empty the directories if they already exists
if (True) :
   # Remove directories
   os.system('rm -r DEMs/bedrock')
   os.system('rm -r DEMs/surface')
   # Create directories
   os.mkdir('DEMs/bedrock')
   os.mkdir('DEMs/surface')


# Now, for every group we create a directory for every inversion model, with all
#the thicknesses of the RGI entities in the group
for group in groups:
    sub_glaciers = shp[shp.glacier_gr == int(group)]
    os.mkdir('DEMs/bedrock/'+group)
    
    for idx, glacier in sub_glaciers.iterrows():
        rgi_id = glacier.RGIId
        print('rgi_id ',rgi_id)
        for model in os.listdir('../../data/thickDEMs'):
            print ('model',model)
            try:
                os.mkdir('DEMs/bedrock/'+group+'/'+model)
            except:
                print()
            # !!! When dowloaded, the MNTs may be named differently between composite and other models
            # if model == 'composite' :
            #    filin = '../../data/thickDEMs/'+model+'/'+rgi_id[:8]+'/'+rgi_id+'_thickness.tif'
            # else :
            #    filin = '../../data/thickDEMs/'+model+'/'+rgi_id[:8]+'/thickness_'+rgi_id+'.tif'
            
            # file names according to Damien Maure dataset
            filin = '../../data/thickDEMs/'+model+'/'+rgi_id[:8]+'/thickness_'+rgi_id+'.tif'
            # output file names
            filou = 'DEMs/bedrock/'+group+'/'+model+'/'+rgi_id+'.tif'
            
            print(filin+' '+filou)
            os.system('cp -v '+filin+' '+filou)
            

# Once everything is ordered in the directories, we call the script to merge the DEMs
#os.system('sh 2.2-merge_dems.sh')
