#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR:   D. MAURE
# ORGANIZATION: IGE(CNRS-France)
#
# VERSION: V1 
# CREATED: 2021-04
# MODIFIED: 
#  
#
#========================================== 
############################

import geopandas as gpd
import os
import sys


#We get the region number via sys
region=sys.argv[1] 
folder_path = "../../data/00_rgi60/"


region_name = str(region) +'_rgi60_'

#We get the exact name of the shapefile for the region considered
for fname in os.listdir(folder_path): 
    print(fname)
    if (region_name in fname) and ~('grouped' in fname):
        region_name = fname
    region_path = folder_path + region_name
    
    
    
shp = gpd.read_file(region_path)

#This creates a Geodataframe with grouped geometries
groups=gpd.GeoDataFrame(geometry=list(shp.unary_union))

#From there, we create an attribute with a group number based on their index
groups['glacier_group']=groups.index+1

#Finally, we add the attribute of the merged entities to the original RGI file 
shp=gpd.tools.sjoin(shp, groups, how='left').drop('index_right',1)

#And we save the resulting shapefile
shp.to_file('rgi_grouped/rgi.shp')

