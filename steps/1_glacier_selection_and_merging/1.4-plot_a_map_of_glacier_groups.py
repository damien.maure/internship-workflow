#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR: Vincent Peyaud
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  Apr 11 2022
#
# OBJECT : Open a shapefile and create shapefiles for each group of glacier
#          (it is then possible to plot it with Qgis)
#
#==========================================

import sys
import os
import geopandas as gpd
import numpy as np
import subprocess

#We get the region number via sys
filin=sys.argv[1]
print("working on ",filin)

### Open the Shapefile ###
sh = gpd.read_file(filin)

### Open a text file   ###
file = open('log.txt', 'w')
print(filin, file=file)

# Save info concerning groups into the text file
print (sh.RGIId.to_list()) # print list of glaciers (ID)
print (sh.glacier_gr.to_list()) # print list of group ID for every glaciers
print ("number of glaciers %4i "%len(sh.RGIId), file=file)
list_gr =  (np.unique(sh.glacier_gr.to_list()))
print ("number of groups %4i "%len(list_gr), file=file)
print ("list of groups : ", file=file)
print (list_gr[:],"\n", file=file)

for i in list_gr :
   print ("group %4i"%i, file=file) # Can be saved in ./rgi_grouped/Correspondance_glacier_group.txt"
   print (sh[sh.glacier_gr==i].RGIId.to_list(), file=file)
   print (sh[sh.glacier_gr==i].Name.to_list(), file=file)
   print (" ", file=file)

### Create Shapefiles for each glacier and create a figure ###

for i in list_gr :
   print (i)
   filou = 'rgi_grouped/all_groups/new_shape_'+str(i)+'.shp'
   print (filou)
   # Select all glaciers from group i and save in a new file
   sh[sh.glacier_gr==i].to_file(filou)
   # Alternative writing for the same command
#   sh[sh['glacier_gr']==i].to_file(filou)

# Create a frigure from Qgis Layout #
# Open the new shapefiles in Qgis
# Open 1.5-mont_blanc_group_layout.qpt to create a layout to picture the location of glacier groups
# For other region : change the coordinate of the figure extent
# Search the folowing line in the 1.5-*.qtp file :
#<Extent xmin="6.70999424218521501" xmax="7.11122357242775838" ymax="46.02577580569287363" ymin="45.74241274084631925"/>
