#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR: Damien Maure and Vincent Peyaud
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  Jan 28 2022
#
# OBJECT : This script selects glaciers in a area (ex. Mont Blanc) and
# belonging to a group > 1 km^2 and save it in a new shapefile
#
#==========================================
import geopandas as gpd
import numpy as np

# Define Area (name and location)
rgi_ID = 11
region = 'MontBlanc'
minlon, maxlon =  6.740,  7.080
minlat, maxlat = 45.740, 46.025
filin = 'rgi_grouped/rgi_'+str(rgi_ID)+'.shp'
filou = 'rgi_grouped/rgi_'+region+'.shp'
filtxt ='rgi_grouped/rgi_'+region+'_infos.txt'
# Define minimal area for the glacier group (km^2)
minarea = 1.0
# Open shapefile created 
sh = gpd.read_file(filin)


# Loop over all glacier groups, remove items with area <  minarea (km^2)
for i in range(4000):
    group=sh[sh.glacier_gr==i]
    tot_area = np.sum(group.Area.tolist()) # sum the area of all glaciers in group i
    if tot_area < minarea :
        sh = sh[sh.glacier_gr != i] # remove group from the list
 #   else :
  #      print(i, sh[sh.glacier_gr==i].RGIId)


#Reshape the shapefile with item inside a geographic location (a box)
sh=sh[sh.CenLon<maxlon][sh.CenLon>minlon][sh.CenLat<maxlat][sh.CenLat>minlat]

print("Shapefile reshaped")
if (False) : # Test print info concerning group Mer de Glace
   print(sh[sh.glacier_gr==844].RGIId) # ex. 844 Group Mer de Glace
   print(sh[sh.glacier_gr==844].CenLon, sh[sh.glacier_gr==844].CenLat)
   print(len(sh))


# Save in a new shapefile
sh.to_file(filou)

# ReOpen shapefile
sh = gpd.read_file(filou)
a = sh.glacier_gr
print ("sh.glacier_gr is :",a)
print("Save info")
# Save info concerning groups
# filinfo = open(filtxt,"w") # format issues =>  COPY MANUALLY PRINT OUTPUT IN A FILE
print (sh.glacier_gr.to_list())
list =  (np.unique(sh.glacier_gr.to_list()))
print ("list of groups : ")
print (list[:])
print ("number of groups %4i "%len(list))

for i in list :
   print ("group %4i"%i) # Can be saved in ./rgi_grouped/Correspondance_glacier_group.txt"
   print (sh[sh.glacier_gr==i].RGIId.to_list())
   print (sh[sh.glacier_gr==i].Name.to_list())
   print ()
#little= sh[sh.Area>1]
#print(len(sh))
