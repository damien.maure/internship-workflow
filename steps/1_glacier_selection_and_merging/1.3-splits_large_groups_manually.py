#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#=========================================
# AUTHOR: Monica Bon Garcia and Vincent Peyaud
# ORGANIZATION: IGE(CNRS-France)
# CREATED:  Apr 06 2022
#
# OBJECT :  This script splits a large group into smaller groups
# Example from the massif du Mont Blanc : the largest group centered around Mont Blanc summit is splited into (Gl. Bossons Bionnassay / Gl. Mer de Glace Lechaux / Gl. Miage / Gl. Brenva)

#
#==========================================

import geopandas as gpd
import subprocess
import numpy as np

#shp_file = '../1_glacier_selection_and_merging/rgi_grouped/rgi_MontBlanc.shp'
filin = './rgi_grouped/rgi_MontBlanc.shp'
filou = './rgi_grouped/rgi_MontBlanc_corrected.shp'

# A possibility is to duplicate the Shapefile (.shp and dependencies)
#subprocess.run('ls -l {}'.format(filin), shell=True, check=True)
#subprocess.run('for fi in rgi_grouped/rgi_MontBlanc.* ; do cp -v ${fi} ${fi%.*}_corrected.${fi##*.} ; done', shell=True, check=True)
# Another possibility is to save the corrected shapefile in a new file (see below)

shp_file = filin

shp = gpd.read_file(shp_file)

lista, listb = [], []
ida, idb = 9998, 9999

lista, ida = [], 9998
# Example for Mont Blanc
for id in ['RGI60-11.03646', 'RGI60-11.03647', 'RGI60-11.03648']: # New group 9998 : Bossons,  Taconnaz, Bionassay
    lista = lista + shp.index[shp['RGIId'] == id].tolist()
    print(lista)

for id in lista:
#    shp['glacier_gr'].loc[id]=ida
    shp.at[id,'glacier_gr']=ida

lista, ida = [], 9999

for id in ['RGI60-11.03642', 'RGI60-11.03643']: # New group 9999 : Leschaux, MdG
	lista = lista + shp.index[shp['RGIId'] == id].tolist()
	print(lista)

for id in lista: 
#	shp['glacier_gr'].loc[id]=idb
	shp.at[id,'glacier_gr']=ida


# Save in a new shapefile
shp.to_file(filou)

