# Step 1 Glacier group outlines processing & selection

The Randolph Glacier Inventory (RGI) is the baseline of the workflow. The entities to be modelled will be selected based on the outlines of the dataset. However, some glacier that are separated in the inventory (for historical or practical reasons) are in reality a single continuous ice body. We thus need to merge them into one entity for Elmer/Ice to work properly.

# FILES:

- **1.1-grouping_shp.py** is the script to run in order to merge the glacier entities. It creates a new attribute for every glacier (*glacier_gr*) that is its *group number* (Every entity in a group shares the same). It need to be run with the RGI region number of the glaciers we want to model as an argument (eg. *01* for Alaska, *11* for Central Europe..).

- **1.2-select-glaciers-goups.py** this script select glaciers in an area (defined in the script by its coodinates). It also removes small glaciers.   

- **glacier_group_list.txt** is the text file that needs to be filled with the *group number* of every group we want to model (this selection can be automated by a script if wanted). It we be called by future steps.

- **rgi_grouped/** is a directory containing the merged rgi shapefile once *grouping_shp.py* has been run.

- **1.3-splits_large_groups_manually.py** splits a large group into smaller groups. This scriptis an example from the massif du Mont Blanc and must be adaptated manually to another configuration.

- **1.4-plot_a_map_of_glacier_groups.py** opens a shapefile and create shapefiles for each group of glacier with their ID.

- **rgi_grouped/all_groups/** is a subdirectory containing the shapefiles for each group of glaciers.

- **1.5-mont_blanc_group_layout.qpt** is a Qgis template to create a layout that print a map of the glacier with their id number (see example in rgi_grouped/Mont_Blanc_groups.png).

# TO PROCESS THE STEP

1. run 
```
1.1-grouping_shp.py region_nb
```

2. run 
```
1.2-select-glaciers-goups.py #
```

3. write the glacier groups you want to model in *glacier_group_list.txt*

4. Optionnaly
```
adapt for a new region then run 1.3-splits_large_groups_manually.py

run 1.4-plot_a_map_of_glacier_groups.py

Open shapefiles for each groups with Qgis then open 1.5-mont_blanc_group_layout.qpt 
```
# Warning
Some glacier groups can be very large: it can rise some problems with Elmer/Ice when solving the Stokes equations. However, it is possible to manually split them where they don't have a lot of interactions (e.g., if glacier join at the top of a ridge).
