# Explanation of stage-m2-damien raw archive, if needed

# Important folders
(I apologize for the dumb names I was stuck with)

**RealSIM/** contains the necessary for the diagnostic runs (calibration of A/C/bedrock vs surface velocities). It is also where most of the Data used was stored (in RealSIM/Data/).
The general organisation is the following:
-*src/* contains all the scripts and useful processing tools
-*dirs/* contains the file tree used by Elmer and the "working" directories
-*Data/* contains the necessary data for the workflow.



**EI_vs_oggm/** contains everything for the transient simulations after calibration with *RealSIM/*.
-*oggm* contains scripts and files used to process smbs from oggm (spinup and rcp) for Elmer/Ice. It also has volume/area evolution calculations scripts for oggm.
-*EI* contaisn everything for Elmer/Ice simulations, ordered as *RealSIM/*.
-*postprocess* contains... postprocess scripts! (for maps or volume evolution on the massif).

**oggm_05degrees/** has everything used to generate the outputs for the Appendix study of my report on "Does 0.5°C matter?"

# Less important folders

**ITMIX** and **ITMIX_v2** were used for the synthetic glacier study

**old_data** was used to temporarily store experiment files